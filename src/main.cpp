#include <Arduino.h>
#include <Wire.h>
#include <DS1307.h>
#include <EEPROM.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
// #include <Servo.h>
#include <ButtonBoard.h>
#include <MemoryFree.h>
#include "tones.h"

//==============================================================================
// Set DS1307 RTC clock object
//==============================================================================
DS1307 clock;

//==============================================================================
// Define I2C communication addresses
//==============================================================================
#define CLOCK_RTC_ADDR 0x68
#define DISPLAY_ADDR 0x3C

//==============================================================================
// Initialize OLED display
//==============================================================================
int screenWidth = 128;
int screenHeight = 64;
Adafruit_SSD1306 display(screenWidth, screenHeight, &Wire, -1);

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Initialize navigation board
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
byte analogPin = A0;
int buttonValues[22][3] = {
	{0, 0, 0}, // button released
	{1, 318, 319}, // button 1
	{2, 205, 206}, // button 2
	{3, 131, 132}, // button 3
	{4, 47, 48}, // button 4
	{5, 18, 20}, // button 5
	{6, 11, 13}, // button 6
	{7, 422, 423}, // button 1 + button 2
	{8, 383, 384}, // button 1 + button 3
	{9, 342, 343}, // button 1 + button 4
	{10, 328, 329}, // button 1 + button 5
	{11, 324, 325}, // button 1 + button 6
	{12, 292, 293}, // button 2 + button 3
	{13, 236, 238}, // button 2 + button 4
	{14, 219, 220}, // button 2 + button 5
	{15, 213, 215}, // button 2 + button 6
	{16, 169, 170}, // button 3 + button 4
	{17, 146, 148}, // button 3 + button 5
	{18, 141, 143}, // button 3 + button 6
	{19, 66, 67}, // button 4 + button 5
	{20, 58, 62}, // button 4 + button 6
	{21, 31, 33} // button 5 + button 6
};
ButtonBoard button(analogPin, buttonValues);

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Forward function declaration
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
byte readByte(byte devaddr, byte memaddr, byte read_bytes);
void clockReset();
void mainScreen();
void menuMovementLogic();
void mainMenu();
void foodMenu();
void moveMenuCursor(int rows);
void callSetup(String type, int feedMenuId);
void setupClock(int pos, int row);
void setupFeed(int pos, int row, int feedMenuId);
int feedTime();
void playMelody();

// Declare reset func (software board reset)
void (* resetFunc) (void) = 0;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// General variales setup
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Display meny marker
int displayMenu = 0;
// Selection cursor positions (horizontal or vertical)
int horCursor = 0;
int verCursor = 0;
// Delay preventing multiple clicks
int menuMoveDelay = 250;
// Delay between value change (clock, date)
int changeDelay = 250;
// Initial clock (if it was never set)
byte clockCheck;
// Feeder memory addresses (minute, hour)
int feeder1Addr[] = {1, 2};
int feeder2Addr[] = {3, 4};
int feeder3Addr[] = {5, 6};
// initialize timers and related values
int hour, minute, second;
int hourMemAddr, minuteMemAddr;
int itIsTime, f1Hour, f1Minute, f2Hour, f2Minute, f3Hour, f3Minute;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Flash memory constant variables (PROGMEM)
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// const char msgDecor[] PROGMEM = "=====================";


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Sound setup constants
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Sound pin
int soundPin = 7;
// Sound speed in miliseconds
int soundSpeed = 1000;
// Pause between notes coefficient. Default +30%
int pauseCoef = 1;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Melody setup
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
int toneSequence[] = {
	_C4, _E4, _G4, _C5
};
int toneDuration[] = {
	2, 2, 2, 1
};

//==============================================================================
//vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
void setup() {
	// Initializes serial console for debugging
	Serial.begin(9600);
	// Initialize for I2C communication
	Wire.begin();
	// Initialize display
	if(!display.begin(SSD1306_SWITCHCAPVCC, DISPLAY_ADDR)) {
		Serial.println(F("SSD1306 allocation failed"));
		for(;;);
	}

	// Set deisplay font properties
	display.setTextSize(1);
	display.setTextColor(WHITE);

	// Check initial clock status
	clockCheck = readByte(CLOCK_RTC_ADDR, 0, 1);

	// Clean display for next screens
	display.clearDisplay();

	// Check free RAM
	Serial.print(F("Free RAM = "));
	Serial.println(freeMemory(), DEC);
}

void loop() {

	// // Debug button input values
	// button.valueDBG();

	if (clockCheck >= 0x80) {
		clockReset();
	} else {
		if (displayMenu == 0) {
			itIsTime = feedTime();
			if (itIsTime == 0) {
				mainScreen();
				menuMovementLogic();
			} else if (itIsTime == 1) {
				playMelody();
			}
		} else if (displayMenu == 1) {
			display.clearDisplay();
			mainMenu();
			display.display();
			menuMovementLogic();
			moveMenuCursor(2);
		} else if (displayMenu == 11) {
			display.clearDisplay();
			callSetup("clock", 0);
			display.display();
			menuMovementLogic();
		} else if (displayMenu == 12) {
			display.clearDisplay();
			foodMenu();
			display.display();
			menuMovementLogic();
			moveMenuCursor(3);
		} else if (121 <= displayMenu && displayMenu <= 123) {
			display.clearDisplay();
			callSetup("food", displayMenu);
			display.display();
			menuMovementLogic();
		}
	}
}
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//==============================================================================




//==============================================================================
// General functions
//==============================================================================

// Control menu navogation options
void redraw() {
	display.clearDisplay();
	delay(menuMoveDelay);
}

// Print leading zero
void printZero(uint8_t tData) {
	if (tData < 10) {
		display.print(F("0"));
		display.print(tData);
	} else {
		display.print(tData);
	}
}

//==============================================================================
// Memory Read/Write operations
//==============================================================================

// Read data from memory (RTC)
byte readByte(byte devaddr, byte memaddr, byte read_bytes) {
	byte data;
	Wire.beginTransmission(devaddr);
	Wire.write(memaddr);
	Wire.endTransmission();
	Wire.requestFrom(devaddr, read_bytes);
	while (Wire.available()) {
		data = Wire.read();
	}
	return data;
}

// Write feed time to EEPROM
void writeFeed(int minute, int hour, int feedMenuId) {

	int hourMemAddr, minuteMemAddr;

	if (feedMenuId == 121) {
		hourMemAddr = feeder1Addr[0];
		minuteMemAddr = feeder1Addr[1];
	} else if (feedMenuId == 122) {
		hourMemAddr = feeder2Addr[0];
		minuteMemAddr = feeder2Addr[1];
	} else if (feedMenuId == 123) {
		hourMemAddr = feeder3Addr[0];
		minuteMemAddr = feeder3Addr[1];
	}

	if (hour > 23) {
		hour = 0;
	}

	if (minute > 59) {
		minute = 0;
	}

	EEPROM.write(hourMemAddr, hour);
	delay(100);
	EEPROM.write(minuteMemAddr, minute);
	delay(100);
}

//==============================================================================
// Print time and feeders
//==============================================================================

void printClock() {
	clock.getTime();
	if (clock.hour < 10) {
		display.print(F("0"));
	}
	display.print(clock.hour, DEC);
	display.print(F(":"));
	if (clock.minute < 10) {
		display.print(F("0"));
	}
	display.print(clock.minute, DEC);
	display.print(F(":"));
	if (clock.second < 10) {
		display.print(F("0"));
	}
	display.print(clock.second, DEC);
}

// Print feed time from RTC EEPROM
void printFeed(int feedMenuId) {

	int hourMemAddr, minuteMemAddr;

	if (feedMenuId == 121) {
		hourMemAddr = feeder1Addr[0];
		minuteMemAddr = feeder1Addr[1];
	} else if (feedMenuId == 122) {
		hourMemAddr = feeder2Addr[0];
		minuteMemAddr = feeder2Addr[1];
	} else if (feedMenuId == 123) {
		hourMemAddr = feeder3Addr[0];
		minuteMemAddr = feeder3Addr[1];
	}

	// Prepare variables
	int feedHour = EEPROM.read(hourMemAddr);
	int feedMinute = EEPROM.read(minuteMemAddr);

	// Print request
	if (feedHour < 10) {
		display.print(F("0"));
	}
	display.print(feedHour, DEC);
	display.print(F(":"));
	if (feedMinute < 10) {
		display.print(F("0"));
	}
	display.print(feedMinute, DEC);
	display.print(F(":00"));
}

//==============================================================================
// Menu, entries and cursors movement and logic
//==============================================================================

// Print main screen with current time and next feed
void mainScreen() {
	display.clearDisplay();
	// Print vertical display separator
	display.setCursor(60, 0);
	display.println(F("|"));
	display.setCursor(60, 8);
	display.println(F("|"));
	display.setCursor(60, 16);
	display.println(F("|"));
	display.setCursor(60, 24);
	display.println(F("|"));
	display.setCursor(60, 32);
	display.println(F("|"));
	display.setCursor(60, 40);
	display.println(F("|"));
	display.setCursor(60, 48);
	display.println(F("|"));
	display.setCursor(60, 56);
	display.println(F("|"));

	// Print feeding timers
	display.setCursor(0, 5);
	display.println(F("Feeding:"));
	display.setCursor(0, 20);
	printFeed(121);
	display.setCursor(0, 30);
	printFeed(122);
	display.setCursor(0, 40);
	printFeed(123);

	// Print current time
	display.setCursor(70, 5);
	display.println(F("Now is:"));
	display.setCursor(70, 20);
	printClock();

	// Command row
	display.setCursor(70, 50);
	display.print(F("MENU"));
	display.setCursor(110, 50);
	display.write(62);
	display.display();
}

// Menu movement logic
void menuMovementLogic() {
	int buttonID = button.setButton();
	if (displayMenu == 0 && buttonID == 5) {
		// Enter main menu
		displayMenu = 1;
		redraw();
	} else if (displayMenu == 1 && buttonID == 6) {
		// Exit to main screen
		displayMenu = 0;
		redraw();
	} else if (displayMenu == 1 && verCursor == 0 && buttonID == 5) {
		// Enter clock setup
		// Exit from menu (save or cancel) come form setup function
		displayMenu = 11;
		clock.getTime();
		hour = clock.hour;
		minute = clock.minute;
		redraw();
	} else if (displayMenu == 1 && verCursor == 10 && buttonID == 5) {
		// Enter food timers menu
		displayMenu = 12;
		redraw();
	} else if (displayMenu == 12 && buttonID == 6) {
		// Exit food timers menu
		displayMenu = 1;
		redraw();
	} else if (displayMenu == 12 && buttonID == 5) {
		// Enter feeder setup
		if (verCursor == 0) {
			displayMenu = 121;
			hourMemAddr = feeder1Addr[0];
			minuteMemAddr = feeder1Addr[1];
		} else if (verCursor == 10) {
			displayMenu = 122;
			hourMemAddr = feeder2Addr[0];
			minuteMemAddr = feeder2Addr[1];
		} else if (verCursor == 20) {
			displayMenu = 123;
			hourMemAddr = feeder3Addr[0];
			minuteMemAddr = feeder3Addr[1];
		}
		hour = EEPROM.read(hourMemAddr);
		minute = EEPROM.read(minuteMemAddr);
		redraw();
	}

	// Set cursor to 0 when move between menus
	if (buttonID == 5 || buttonID == 6) {
		verCursor = 0;
	}
}

// Menu cursor movement
void moveMenuCursor(int rows) {
	int buttonID = button.setButton();
	if (buttonID == 3 && verCursor < ((rows * 10) - 10)) {
		verCursor += 10;
		redraw();
	} else if (buttonID == 2 && verCursor > 0) {
		verCursor -= 10;
		redraw();
	}
}

// Print main menu
void mainMenu() {
	display.setCursor(0, verCursor);
	display.write(16);
	// option1
	display.setCursor(10, 0);
	display.print(F("Clock setup"));
	display.setCursor(110, 0);
	display.write(24);
	// option2
	display.setCursor(10, 10);
	display.print(F("Feed setup"));
	display.setCursor(110, 10);
	display.write(25);
	// Command row
	display.setCursor(0, 55);
	display.write(60);
	display.print(F(" BACK"));
	display.setCursor(80, 55);
	display.print(F("SELECT "));
	display.write(62);
}

// Print food time menu
void foodMenu() {
	display.setCursor(0, verCursor);
	display.write(16);
	// option1
	display.setCursor(10, 0);
	display.print(F("Feed 1 time"));
	display.setCursor(110, 0);
	display.write(24);
	// option2
	display.setCursor(10, 10);
	display.print(F("Feed 2 time"));
	// option3
	display.setCursor(10, 20);
	display.print(F("Feed 3 time"));
	display.setCursor(110, 20);
	display.write(25);
	// Command row
	display.setCursor(0, 55);
	display.write(60);
	display.print(F(" BACK"));
	display.setCursor(80, 55);
	display.print(F("SELECT "));
	display.write(62);
}

void callSetup(String type, int feedMenuId=0) {
	display.setCursor(0, 0);
	// Call setup command
	if ((type == "clock") && (feedMenuId == 0)) {
		display.print(F("==== Clock setup ===="));
		setupClock(40, 15);
	} else if ((type == "food") && (feedMenuId != 0)) {
		int feedId = feedMenuId % 121 + 1;
		display.print(F("=== Feed "));
		display.print(feedId);
		display.print(F(" config ==="));
		setupFeed(40, 15, feedMenuId);
	}
	// Command row
	display.setCursor(0, 55);
	display.write(60);
	display.print(F(" CANCEL"));
	display.setCursor(90, 55);
	display.print(F("SAVE "));
	display.write(62);
}

void clockReset() {
	display.clearDisplay();
	display.setCursor(0, 0);
	display.print(F("===================="));
	display.setCursor(22, 15);
	display.print(F("Reset clock..."));
	display.setCursor(0, 55);
	display.print(F("====================="));
	display.display();
	delay(3000);
	clock.stopClock();
	clock.fillByYMD(2008,1,29);           //YYYY,MM,DD
	clock.fillByHMS(18,30,00);            //hh:mm:ss
	clock.fillDayOfWeek(TUE);             //MON,TUE,WED,THU,FRI,SAT,SUN
	clock.setTime();
	clock.startClock();
	resetFunc();
}

//==============================================================================
// Clock configuration
//==============================================================================

// Clock setup screen
void setupClock(int pos, int row) {

	clock.stopClock();
	int valPos = pos;
	int buttonID = button.setButton();

	if (horCursor == 0) {
		// Increase/Decrease hours
		if (buttonID == 2) {
			if (hour == 23) {
				hour = 0;
			} else {
				hour = hour + 1;
			}
			delay(changeDelay);
		} else if (buttonID == 3) {
			if (hour == 0) {
				hour = 23;
			} else {
				hour = hour - 1;
			}
			delay(changeDelay);
		}
	} else if (horCursor == 1) {
		// Increase/Decrease minutes
		if (buttonID == 2) {
			if (minute == 59) {
				minute = 0;
			} else {
				minute = minute + 1;
			}
			delay(changeDelay);
		} else if (buttonID == 3) {
			if (minute == 0) {
				minute = 59;
			} else {
				minute = minute - 1;
			}
			delay(changeDelay);
		}
	}

	if (buttonID == 1) {
		// Move cursor to hours
		horCursor = 0;
		redraw();
	} else if (buttonID == 4) {
		// Move cursor to minutes
		horCursor = 1;
		redraw();
	} else if (buttonID == 5) {
		// Exit clock setup (Save to RTC)
		displayMenu = 0;
		clock.fillByHMS(hour,minute,0);
		clock.setTime();
		clock.startClock();
		redraw();
	} else if (buttonID == 6) {
		// Exit clock setup (Cancel)
		displayMenu = 1;
		redraw();
	}

	// Print hours
	display.setCursor(valPos, row);
	if (hour < 10) {
		display.print(F("0"));
	}
	display.print(hour, DEC);
	// Print separator
	valPos = valPos + 12;
	display.setCursor(valPos, row);
	display.print(F(":"));
	// Print minutes
	valPos = valPos + 6;
	display.setCursor(valPos, row);
	if (minute < 10) {
		display.print(F("0"));
	}
	display.print(minute, DEC);
	// Print separator
	valPos = valPos + 12;
	display.setCursor(valPos, row);
	display.print(F(":"));
	// Print seconds
	valPos = valPos + 6;
	display.setCursor(valPos, row);
	display.print(F("00"));

	// Print selection cursor
	if (horCursor == 0) {
		display.setCursor(pos, (row + 10));
		display.print(F("^^"));
	} else if (horCursor == 1) {
		display.setCursor((pos + 18), (row + 10));
		display.print(F("^^"));
	}

	// Print arrows
	display.setCursor(100, row);
	display.write(27);
	display.write(18);
	display.write(26);
}

void setupFeed(int pos, int row, int feedMenuId) {

	int valPos = pos;
	int buttonID = button.setButton();

	if (horCursor == 0) {
		if (buttonID == 2) {
			if (hour == 23) {
				hour = 0;
			} else {
				hour = hour + 1;
			}
			delay(changeDelay);
		} else if (buttonID == 3) {
			if (hour == 0) {
				hour = 23;
			} else {
				hour = hour - 1;
			}
			delay(changeDelay);
		}
	} else if (horCursor == 1) {
		if (buttonID == 2) {
			if (minute == 59) {
				minute = 0;
			} else {
				minute = minute + 1;
			}
			delay(changeDelay);
		} else if (buttonID == 3) {
			if (minute == 0) {
				minute = 59;
			} else {
				minute = minute - 1;
			}
			delay(changeDelay);
		}
	}

	// Move horizontal cursor
	if (buttonID == 1) {
		horCursor = 0;
		redraw();
	} else if (buttonID == 4) {
		horCursor = 1;
		redraw();
	} else if (buttonID == 5) {
		// Exit feed setup (Save to EEPROM)
		displayMenu = 12;
		writeFeed(minute, hour, feedMenuId);
		redraw();
	} else if (buttonID == 6) {
		// Exit feed setup (Cancel)
		displayMenu = 12;
		redraw();
	}

	// Print hours
	display.setCursor(valPos, row);
	printZero(hour);
	// Print separator
	valPos = valPos + 12;
	display.setCursor(valPos, row);
	display.print(F(":"));
	// Print minutes
	valPos = valPos + 6;
	display.setCursor(valPos, row);
	printZero(minute);
	// Print separator
	valPos = valPos + 12;
	display.setCursor(valPos, row);
	display.print(F(":"));
	// Print seconds
	valPos = valPos + 6;
	display.setCursor(valPos, row);
	display.print(F("00"));

	// Print selection cursor
	if (horCursor == 0) {
		display.setCursor(pos, (row + 10));
		display.print(F("^^"));
	} else if (horCursor == 1) {
		display.setCursor((pos + 18), (row + 10));
		display.print(F("^^"));
	}

	// Print arrows
	display.setCursor(100, row);
	display.write(27);
	display.write(18);
	display.write(26);
}

//==============================================================================
// Time for food
//==============================================================================

// Load and compare feed timers
int feedTime() {

	int timeMarker = 0;

	// Get feeding time from EEPROM
	int f1Hour = EEPROM.read(feeder1Addr[0]);
	int f1Minute = EEPROM.read(feeder1Addr[1]);
	int f2Hour = EEPROM.read(feeder2Addr[0]);
	int f2Minute = EEPROM.read(feeder2Addr[1]);
	int f3Hour = EEPROM.read(feeder3Addr[0]);
	int f3Minute = EEPROM.read(feeder3Addr[1]);

	// Get current time from RTC
	clock.getTime();
	hour = clock.hour;
	minute = clock.minute;
	second = clock.second;

	if (((hour == f1Hour) && (minute == f1Minute) && (second == 0)) ||
	    ((hour == f2Hour) && (minute == f2Minute) && (second == 0)) ||
	    ((hour == f3Hour) && (minute == f3Minute) && (second == 0))) {
		timeMarker = 1;
	}

	return timeMarker;
}

// Play feeder melody
void playMelody() {
	int melodyLength = (sizeof(toneSequence) / sizeof(toneSequence[0]));
	for (int melodyNote = 0; melodyNote < melodyLength; melodyNote++) {
		int noteDuration = soundSpeed / toneDuration[melodyNote];
		tone(soundPin, toneSequence[melodyNote], noteDuration);
		int pauseBetweenNotes = noteDuration * pauseCoef;
		delay(pauseBetweenNotes);
		noTone(soundPin);
	}
}
