#include <Arduino.h>
#include <Wire.h>
#include <LiquidCrystal.h>
#include <Servo.h>
#include <NavBoard.h>
#include "byte_arrows.h"
#include "tones.h"

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Initialize RTC and parameters
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// I2C  communication addresses
#define CLOCK_EEPROM_ADDR 0x57
#define CLOCK_RTCC_ADDR 0x6f
// RTC valid bits
int vbitSecond = 7;
int vbitMinute = 7;
int vbitHour = 6;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Initialize and setup display
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);
int displayColumns = 20;
int displayRows = 4;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Initialize servo motor and parameters
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Servo myservo;
int servoPosition;
int servoValveStepDelay = 15;
int servoValveMaxAngle = 180;
int servoValveStepDegree = 1;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Initialize navigation board
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
byte analogPin = A0;
NavBoard board(analogPin, 90, 130, 160, 190, 220, 250);

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Forward function declaration
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
byte readByte(byte devaddr, byte memaddr, byte read_bytes);
void loadFeed();
void clockReset();
void mainScreen();
void menuMovementLogic();
void mainMenu();
void foodMenu();
void moveCursor(int rows);
void clockSetup();
void foodSetup(int fid);
void clockConfig(int pos, int row);
int feedTime();
void playMelody();
void catFeeding(int stepDelay, int valveAngle, int valveStepDegree);
void valveInit();
// Declare reset func (software board reset)
void (* resetFunc) (void) = 0;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// General variales setup
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Display meny marker
int displayMenu = 0;
// Selection cursor positions (horizontal or vertical)
int horCursor = 0;
int verCursor = 0;
// Delay preventing multiple clicks
int menuMoveDelay = 100;
// Delay between value change (clock, date)
int changeDelay = 250;
// Initial clock (if it was never set)
byte clockCheck;
// minute, hour
int initClock[] = {0, 0};
// Feeder addresses (minute, hour)
int feeder1Addr[] = {11, 12};
int feeder2Addr[] = {21, 22};
int feeder3Addr[] = {31, 32};
// Create empty values for time elements
int SECOND, MINUTE, HOUR;
// Food Timers
int f1Minute, f1Hour, f2Minute, f2Hour, f3Minute, f3Hour;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Sound setup
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Sound pin
int soundPin = 7;
// Sound speed in miliseconds
int soundSpeed = 1000;
// Pause between notes coefficient. Default +30%
int pauseCoef = 1;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Melody setup
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
int toneSequence[] = {
	_C4, _E4, _G4, _C5
};
int toneDuration[] = {
	2, 2, 2, 1
};

//==============================================================================
//vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
void setup() {
	// Initializes serial console for debugging
	Serial.begin(9600);
	// Initialize for I2C communication
	Wire.begin();
	// Set servo control digital pin
	myservo.attach(9);
	// Initializes and clears the LCD screen
	lcd.begin(displayColumns, displayRows);
	// Initialize to listen for button click
	pinMode(A0, INPUT_PULLUP);
	// Initialize custom characters
	lcd.createChar(0, arrowBack);
	lcd.createChar(1, arrowLeft);
	lcd.createChar(2, arrowDown);
	lcd.createChar(3, arrowUp);
	lcd.createChar(4, arrowRight);
	lcd.createChar(5, arrowSelect);
	lcd.createChar(6, selectionCursor);
	lcd.createChar(7, arrowUpDown);
	// Check initial clock status
	clockCheck = readByte(CLOCK_RTCC_ADDR, 0, 1);
	// Load feeding timers from RTC EEPROM
	loadFeed();
	// Initialize servo valve to 0 (closed)
	valveInit();
}

void loop() {

	if (clockCheck < 0x80) {
		clockReset();
	} else {
		if (displayMenu == 0) {
			mainScreen();
			menuMovementLogic();
		} else if (displayMenu == 1) {
			mainMenu();
			menuMovementLogic();
			moveCursor(2);
		} else if (displayMenu == 11) {
			clockSetup();
			menuMovementLogic();
		} else if (displayMenu == 12) {
			foodMenu();
			menuMovementLogic();
			moveCursor(3);
		} else if (121 <= displayMenu && displayMenu <= 123) {
			foodSetup(displayMenu);
			menuMovementLogic();
		}
	}
}
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//==============================================================================




//==============================================================================
// General functions
//==============================================================================

// Redraw screen on button click (menu navigation)
void redraw() {
	lcd.clear();
	delay(menuMoveDelay);
}

// Convert decimal hour to binary coded hour
uint8_t dec2hex(uint8_t num) {
	uint8_t units = num % 10;
	uint8_t tens = num / 10;
	return (tens << 4) | units;
}

// Convert binary coded hour to decimal hour
uint8_t hex2dec(uint8_t num) {
	uint8_t units = num & 0x0F;
	uint8_t tens = num >> 4;
	return tens*10 + units;
}

// Convert binary coded hour to decimal hour
uint8_t rtc2print(uint8_t num, int validBits) {
	return num & 0xff >> (8-validBits);
}

// Print leading zero
void printZero(uint8_t tData) {
	if (tData < 10) {
		lcd.print("0");
		lcd.print(tData);
	} else {
		lcd.print(tData);
	}
}

// Convert integer to bitmask
byte bitmask(int validBits) {
	byte mask = 0b0;
	for (int i = 1; i <= validBits; i++) {
		mask = (mask << 1) + 1;
	}
	return mask;
}

//==============================================================================
// Memory Read/Write operations
//==============================================================================

// Read data from memory (RTCC or EEPROM)
byte readByte(byte devaddr, byte memaddr, byte read_bytes) {
	byte data;
	Wire.beginTransmission(devaddr);
	Wire.write(memaddr);
	Wire.endTransmission();
	Wire.requestFrom(devaddr, read_bytes);
	while (Wire.available()) {
		data = Wire.read();
	}
	return data;
}

// Write data to memory (RTCC or EEPROM)
void writeByte(byte devaddr, byte memaddr, byte data) {
	Wire.beginTransmission(devaddr);
	Wire.write(memaddr);
	Wire.write(data);
	Wire.endTransmission();
}

// Read clock  from RTC SRAM
void readClock() {
	byte rawByte;

	// Prepare variables
	rawByte = readByte(CLOCK_RTCC_ADDR, 0, 1);
	SECOND = hex2dec(rawByte & bitmask(vbitSecond));
	rawByte = readByte(CLOCK_RTCC_ADDR, 1, 1);
	MINUTE = hex2dec(rawByte & bitmask(vbitMinute));
	rawByte = readByte(CLOCK_RTCC_ADDR, 2, 1);
	HOUR = hex2dec(rawByte & bitmask(vbitHour));
}

// Write clock to RTCC
void writeClock(int arr[]) {
	writeByte(CLOCK_RTCC_ADDR, 0, 0);                //STOP RTC (set byte to 0)
	writeByte(CLOCK_RTCC_ADDR, 1, dec2hex(arr[0]));  //set minutes
	writeByte(CLOCK_RTCC_ADDR, 2, dec2hex(arr[1]));  //set hours
	writeByte(CLOCK_RTCC_ADDR, 3, 0x09);             //set VBAT (to use backup battery)
	writeByte(CLOCK_RTCC_ADDR, 0, 0x80);             //START RTC, SECOND=00 (set byte to 128)
	delay(100);
}

// Read feed time from RTC EEPROM
void readFeed(int feedMenuId) {
	byte rawByte;
	int minuteAddress, hourAddress;

	if (feedMenuId == 121) {
		minuteAddress = feeder1Addr[0];
		hourAddress = feeder1Addr[1];
	} else if (feedMenuId == 122) {
		minuteAddress = feeder2Addr[0];
		hourAddress = feeder2Addr[1];
	} else if (feedMenuId == 123) {
		minuteAddress = feeder3Addr[0];
		hourAddress = feeder3Addr[1];
	}

	rawByte = readByte(CLOCK_EEPROM_ADDR, minuteAddress, 1);
	MINUTE = hex2dec(rawByte);
	rawByte = readByte(CLOCK_EEPROM_ADDR, hourAddress, 1);
	HOUR = hex2dec(rawByte);
}

// Load feed timers for comparsion
void loadFeed() {
	byte rawByte;
	int minuteAddress, hourAddress, printMinute, printHour;
	int feederCount = 3;

	for (int i = 1; i <= feederCount; i++) {
		if (i == 1) {
			minuteAddress = feeder1Addr[0];
			hourAddress = feeder1Addr[1];
		} else if (i == 2) {
			minuteAddress = feeder2Addr[0];
			hourAddress = feeder2Addr[1];
		} else if (i == 3) {
			minuteAddress = feeder3Addr[0];
			hourAddress = feeder3Addr[1];
		}

		// Prepare variables
		rawByte = readByte(CLOCK_EEPROM_ADDR, minuteAddress, 1);
		printMinute = hex2dec(rawByte);
		rawByte = readByte(CLOCK_EEPROM_ADDR, hourAddress, 1);
		printHour = hex2dec(rawByte);

		if (i == 1) {
			f1Minute = printMinute;
			f1Hour = printHour;
		} else if (i == 2) {
			f2Minute = printMinute;
			f2Hour = printHour;
		} else if (i == 3) {
			f3Minute = printMinute;
			f3Hour = printHour;
		}
	}
}

// Write feed time to RTC EEPROM
void writeFeed(int minute, int hour, int feedMenuId) {
	int hourAddress;
	int minuteAddress;
	if (feedMenuId == 121) {
		minuteAddress = feeder1Addr[0];
		hourAddress = feeder1Addr[1];
	} else if (feedMenuId == 122) {
		minuteAddress = feeder2Addr[0];
		hourAddress = feeder2Addr[1];
	} else if (feedMenuId == 123) {
		minuteAddress = feeder3Addr[0];
		hourAddress = feeder3Addr[1];
	}

	if (hour > 23) {
		hour = 0;
	}

	if (minute > 59) {
		minute = 0;
	}

	writeByte(CLOCK_EEPROM_ADDR, minuteAddress, dec2hex(minute));
	delay(100);
	writeByte(CLOCK_EEPROM_ADDR, hourAddress, dec2hex(hour));
	delay(100);
}

//==============================================================================
// Print time and feeders
//==============================================================================

// Print clock from RTC SRAM
void printClock() {
	byte rawByte;

	int printSecond, printMinute, printHour;

	// Prepare variables
	rawByte = readByte(CLOCK_RTCC_ADDR, 0, 1);
	printSecond = rtc2print(rawByte, vbitSecond);
	rawByte = readByte(CLOCK_RTCC_ADDR, 1, 1);
	printMinute = rtc2print(rawByte, vbitMinute);
	rawByte = readByte(CLOCK_RTCC_ADDR, 2, 1);
	printHour = rtc2print(rawByte, vbitHour);

	// Print request
	if (printHour < 10) {
		lcd.print("0");
	}
	lcd.print(printHour, HEX);
	lcd.print(":");
	if (printMinute < 10) {
		lcd.print("0");
	}
	lcd.print(printMinute, HEX);
	lcd.print(":");
	if (printSecond < 10) {
		lcd.print("0");
	}
	lcd.print(printSecond, HEX);
}

// Print feed time from RTC EEPROM
void printFeed(int feedMenuId) {
	byte rawByte;
	int minuteAddress, hourAddress, printMinute, printHour;

	if (feedMenuId == 121) {
		minuteAddress = feeder1Addr[0];
		hourAddress = feeder1Addr[1];
	} else if (feedMenuId == 122) {
		minuteAddress = feeder2Addr[0];
		hourAddress = feeder2Addr[1];
	} else if (feedMenuId == 123) {
		minuteAddress = feeder3Addr[0];
		hourAddress = feeder3Addr[1];
	}

	// Prepare variables
	rawByte = readByte(CLOCK_EEPROM_ADDR, minuteAddress, 1);
	printMinute = hex2dec(rawByte);
	rawByte = readByte(CLOCK_EEPROM_ADDR, hourAddress, 1);
	printHour = hex2dec(rawByte);

	// Print request
	if (printHour < 10) {
		lcd.print("0");
	}
	lcd.print(printHour, DEC);
	lcd.print(":");
	if (printMinute < 10) {
		lcd.print("0");
	}
	lcd.print(printMinute, DEC);
	lcd.print(":00");
}

//==============================================================================
// Menu, entries and cursors movement and logic
//==============================================================================

// Print main screen with current time and next feed
void mainScreen() {
	lcd.setCursor(0, 0);
	lcd.print("Feeding: =----------");
	lcd.setCursor(0, 1);
	printFeed(121);
	lcd.setCursor(9, 1);
	lcd.print("= ");
	printClock();
	lcd.setCursor(0, 2);
	printFeed(122);
	lcd.setCursor(9, 2);
	lcd.print("=----------");
	// Command row
	lcd.setCursor(0, 3);
	printFeed(123);
	lcd.setCursor(9, 3);
	lcd.print("=  MENU");
	lcd.setCursor(17, 3);
	lcd.write(byte(7));

	int itIsTime = feedTime();
	if (itIsTime == 1) {
		playMelody();
		catFeeding(servoValveStepDelay, servoValveMaxAngle, servoValveStepDegree);
	}
}

// Menu movement logic
void menuMovementLogic() {
	int button = board.setButton();
	if (displayMenu == 0 && (button == 3 || button == 4)) {
		// Enter main menu
		displayMenu = 1;
		redraw();
	} else if (displayMenu == 1 && button == 1) {
		// Exit to main screen
		displayMenu = 0;
		redraw();
	} else if (displayMenu == 1 && verCursor == 0 && button == 6) {
		// Enter clock setup
		displayMenu = 11;
		readClock();
		redraw();
	} else if (displayMenu == 11 && button == 1) {
		// Exit clock setup (cancel)
		displayMenu = 1;
		redraw();
	} else if (displayMenu == 11 && button == 6) {
		// Exit clock setup (saved)
		displayMenu = 0;
		int data[] = {MINUTE, HOUR};
		writeClock(data);
		redraw();
	} else if (displayMenu == 1 && verCursor == 1 && button == 6) {
		// Enter food timers menu
		displayMenu = 12;
		redraw();
	} else if (displayMenu == 12 && button == 1) {
		// Exit food timers menu
		displayMenu = 1;
		redraw();
	} else if (displayMenu == 12 && button == 6) {
		// Enter feeder setup
		if (verCursor == 0) {
			displayMenu = 121;
		} else if (verCursor == 1) {
			displayMenu = 122;
		} else if (verCursor == 2) {
			displayMenu = 123;
		}
		readFeed(displayMenu);
		redraw();
	} else if ((121 <= displayMenu && displayMenu <= 123) && button == 1) {
		// Exit feeder setup (cancel)
		displayMenu = 12;
		redraw();
	} else if ((121 <= displayMenu && displayMenu <= 123) && button == 6) {
		// Exit feeder setup (saved)
		writeFeed(MINUTE, HOUR, displayMenu);
		displayMenu = 12;
		loadFeed();
		redraw();
	}

	// Set cursor to 0 when move between menus
	if (button == 1 || button == 6) {
		verCursor = 0;
	}
}

// Move selection cursor between menu options
void moveCursor(int rows) {
	int button = board.setButton();
	if (button == 4 && verCursor < (rows - 1)) {
		verCursor++;
		redraw();
	} else if (button == 3 && verCursor > 0) {
		verCursor--;
		redraw();
	}
}

// Print main menu
void mainMenu() {
	lcd.setCursor(0, verCursor);
	lcd.write(byte(6));
	// option1
	lcd.setCursor(1, 0);
	lcd.print("Clock setup");
	lcd.setCursor(19, 0);
	lcd.write(byte(3));
	// option2
	lcd.setCursor(1, 1);
	lcd.print("Feed setup");
	lcd.setCursor(19, 1);
	lcd.write(byte(2));
	// Command row
	lcd.setCursor(0, 3);
	lcd.write(byte(0));
	lcd.print(" BACK");
	lcd.setCursor(12, 3);
	lcd.print("SELECT ");
	lcd.write(byte(5));
}

// Print food time menu
void foodMenu() {
	lcd.setCursor(0, verCursor);
	lcd.write(byte(6));
	// option1
	lcd.setCursor(1, 0);
	lcd.print("Feed 1 time");
	lcd.setCursor(19, 0);
	lcd.write(byte(3));
	// option2
	lcd.setCursor(1, 1);
	lcd.print("Feed 2 time");
	// option3
	lcd.setCursor(1, 2);
	lcd.print("Feed 3 time");
	lcd.setCursor(19, 2);
	lcd.write(byte(2));
	// Command row
	lcd.setCursor(0, 3);
	lcd.write(byte(0));
	lcd.print(" BACK");
	lcd.setCursor(12, 3);
	lcd.print("SELECT ");
	lcd.write(byte(5));
}

void clockSetup() {
	lcd.setCursor(0, 0);
	lcd.print("==== Clock setup ===");
	// Time selector
	clockConfig(6, 1);
	// Command row
	lcd.setCursor(0, 3);
	lcd.write(byte(0));
	lcd.print(" CANCEL");
	lcd.setCursor(14, 3);
	lcd.print("SAVE ");
	lcd.write(byte(5));
}

// Print clock setup option
void foodSetup(int feedMenuId) {
	int feedId = feedMenuId % 121 + 1;
	lcd.setCursor(0, 0);
	lcd.print("== Feed  ");
	lcd.print(feedId);
	lcd.print(" config ==");
	// Time selector
	clockConfig(6, 1);
	// Command row
	lcd.setCursor(0, 3);
	lcd.write(byte(0));
	lcd.print(" CANCEL");
	lcd.setCursor(14, 3);
	lcd.print("SAVE ");
	lcd.write(byte(5));
}

void clockReset() {
	lcd.setCursor(0, 0);
	lcd.print("====================");
	lcd.setCursor(3, 1);
	lcd.print("Reset clock...");
	lcd.setCursor(0, 3);
	lcd.print("====================");
	delay(3000);
	writeClock(initClock);
	resetFunc();
}

//==============================================================================
// Clock configuration
//==============================================================================

void clockConfig(int pos, int row) {
	int valPos = pos;

	// Manipulate clock
	int button = board.setButton();
	if (horCursor == 0) {
		if (button == 3) {
			if (HOUR == 23) {
				HOUR = 0;
			} else {
				HOUR = HOUR + 1;
			}
			delay(changeDelay);
		} else if (button == 4) {
			if (HOUR == 0) {
				HOUR = 23;
			} else {
				HOUR = HOUR - 1;
			}
			delay(changeDelay);
		}
	} else if (horCursor == 1) {
		if (button == 3) {
			if (MINUTE == 59) {
				MINUTE = 0;
			} else {
				MINUTE = MINUTE + 1;
			}
			delay(changeDelay);
		} else if (button == 4) {
			if (MINUTE == 0) {
				MINUTE = 59;
			} else {
				MINUTE = MINUTE - 1;
			}
			delay(changeDelay);
		}
	}

	// Move horizontal cursor
	if (button == 2) {
		horCursor = 0;
		redraw();
	} else if (button == 5) {
		horCursor = 1;
		redraw();
	}

	// Print hours
	lcd.setCursor(valPos, row);
	printZero(HOUR);
	// Print separator
	valPos = valPos + 2;
	lcd.setCursor(valPos, row);
	lcd.print(":");
	// Print minutes
	valPos = valPos + 1;
	lcd.setCursor(valPos, row);
	printZero(MINUTE);
	// Print separator
	valPos = valPos + 2;
	lcd.setCursor(valPos, row);
	lcd.print(":");
	// Print seconds
	valPos = valPos + 1;
	lcd.setCursor(valPos, row);
	lcd.print("00");

	// Print selection cursor
	if (horCursor == 0) {
		lcd.setCursor(pos, (row + 1));
		lcd.print("^^");
	} else if (horCursor == 1) {
		lcd.setCursor((pos + 3), (row + 1));
		lcd.print("^^");
	}

	// Print arrows
	lcd.setCursor(17, row);
	lcd.write(byte(1));
	lcd.write(byte(7));
	lcd.write(byte(4));
}

//==============================================================================
// Time for food
//==============================================================================

// Load and compare feed timers
int feedTime() {
	int timeMarker = 0;

	readClock();

	if (((HOUR == f1Hour) && (MINUTE == f1Minute) && (SECOND == 0)) ||
	    ((HOUR == f2Hour) && (MINUTE == f2Minute) && (SECOND == 0)) ||
	    ((HOUR == f3Hour) && (MINUTE == f3Minute) && (SECOND == 0))) {
		timeMarker = 1;
	}

	return timeMarker;
}

// Play feeder melody
void playMelody() {
	int melodyLength = (sizeof(toneSequence) / sizeof(toneSequence[0]));
	for (int melodyNote = 0; melodyNote < melodyLength; melodyNote++) {
		int noteDuration = soundSpeed / toneDuration[melodyNote];
		tone(soundPin, toneSequence[melodyNote], noteDuration);
		int pauseBetweenNotes = noteDuration * pauseCoef;
		delay(pauseBetweenNotes);
		noTone(soundPin);
	}
}

// Open and close feeder valve
void catFeeding(int stepDelay, int valveAngle, int valveStepDegree) {
	// Open feeder valve
  for (servoPosition = 0; servoPosition <= valveAngle; servoPosition += valveStepDegree) {
    myservo.write(servoPosition);
    delay(stepDelay);
  }
	// Close feeder valve
  for (servoPosition = valveAngle; servoPosition >= 0; servoPosition -= valveStepDegree) {
    myservo.write(servoPosition);
    delay(stepDelay);
  }
}

// Close feeder valve
void valveInit() {
  for (servoPosition = 180; servoPosition >= 0; servoPosition -= 45) {
    myservo.write(servoPosition);
    delay(15);
  }
}
