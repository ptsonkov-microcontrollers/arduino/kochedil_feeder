#include <Arduino.h>

// Define custom characters (arrows) for menu
byte arrowBack[8] = {
	B00000,
	B11110,
	B00001,
	B00101,
	B01101,
	B11110,
	B01100,
	B00100
};

byte arrowLeft[8] = {
	B00000,
	B00000,
	B00100,
	B01100,
	B11100,
	B01100,
	B00100,
	B00000
};

byte arrowDown[8] = {
	B00000,
	B00000,
	B00000,
	B00000,
	B00000,
	B11111,
	B01110,
	B00100
};

byte arrowUp[8] = {
	B00100,
	B01110,
	B11111,
	B00000,
	B00000,
	B00000,
	B00000,
	B00000
};

byte arrowRight[8] = {
	B00000,
	B00000,
	B00100,
	B00110,
	B00111,
	B00110,
	B00100,
	B00000
};

byte arrowSelect[8] = {
	B00000,
	B10000,
	B10000,
	B10100,
	B10110,
	B11111,
	B00110,
	B00100
};

byte selectionCursor[8] = {
	B00000,
	B00100,
	B11110,
	B11111,
	B11111,
	B11110,
	B00100,
	B00000
};

byte arrowUpDown[8] = {
	B00100,
  B01110,
  B11111,
  B00000,
  B00000,
  B11111,
  B01110,
  B00100
};
